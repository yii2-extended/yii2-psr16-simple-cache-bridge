<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-psr16-simple-cache-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Yii2SimpleCache;

use Closure;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Stringable;
use yii\caching\Dependency;

/**
 * Yii2ToPsr16SimpleCache class file.
 * 
 * This class transforms yii2 compliant cache calls to psr-16 compliant simple
 * cache calls.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class Yii2ToPsr16SimpleCache implements \yii\caching\CacheInterface, Stringable
{
	
	/**
	 * The psr-16 compliant cache.
	 * 
	 * @var CacheInterface
	 */
	protected CacheInterface $_psr16Cache;
	
	/**
	 * Builds a new Yii2ToPsr16SimpleCache object with the given inner yii2 
	 * cache.
	 * 
	 * @param CacheInterface $cache
	 */
	public function __construct(CacheInterface $cache)
	{
		$this->_psr16Cache = $cache;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::buildKey()
	 * @param null|boolean|integer|float|string|object|resource|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $key
	 * @return string
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function buildKey($key) : string
	{
		if(\is_array($key))
		{
			return \md5(\implode(':', \array_keys($key)));
		}
		
		if(\is_object($key))
		{
			return \md5(\spl_object_hash($key).\serialize($key));
		}
		
		if(\is_resource($key))
		{
			return \md5(\get_resource_type($key));
		}
		
		$strkey = (string) $key;
		if(\preg_match('#^[A-Za-z0-9_/+=-]+$#', $strkey))
		{
			return $strkey;
		}
		
		return \md5((string) $key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::get()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $key
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType,MixedInferredReturnType
	 */
	public function get($key) : mixed
	{
		/** @psalm-suppress MixedReturnStatement */
		return $this->_psr16Cache->get($this->buildKey($key));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::exists()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $key
	 * @return boolean
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function exists($key) : bool
	{
		return $this->_psr16Cache->has($this->buildKey($key));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::multiGet()
	 * @param array<integer|string, string> $keys
	 * @return array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @throws InvalidArgumentException
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	public function multiGet($keys) : array
	{
		$newkeys = [];
		
		foreach($keys as $key)
		{
			$newkeys[] = $this->buildKey($key);
		}
		
		/** @psalm-suppress MixedReturnTypeCoercion */
		return (array) $this->_psr16Cache->getMultiple($newkeys);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::set()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $key
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param ?int $duration
	 * @param ?Dependency $dependency
	 * @return boolean
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function set($key, $value, $duration = null, $dependency = null) : bool
	{
		return $this->_psr16Cache->set($this->buildKey($key), $value, $duration);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::multiSet()
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $items
	 * @param int $duration
	 * @param ?Dependency $dependency
	 * @return array<integer, string>
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function multiSet($items, $duration = 0, $dependency = null) : array
	{
		$failed = [];
		
		foreach($items as $key => $item)
		{
			$res = $this->set($this->buildKey($key), $item, $duration, $dependency);
			if(!$res)
			{
				$failed[] = (string) $key;
			}
		}
		
		return $failed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::add()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $key
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param int $duration
	 * @param ?Dependency $dependency
	 * @return boolean
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function add($key, $value, $duration = 0, $dependency = null) : bool
	{
		$key = $this->buildKey($key);
		if(!$this->exists($key))
		{
			$this->set($key, $value, $duration, $dependency);
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::multiAdd()
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $items
	 * @param int $duration
	 * @param ?Dependency $dependency
	 * @return array<integer, string>
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function multiAdd($items, $duration = 0, $dependency = null) : array
	{
		$failed = [];
		
		foreach($items as $key => $item)
		{
			$res = $this->add($this->buildKey($key), $item, $duration, $dependency);
			if(!$res)
			{
				$failed[] = (string) $key;
			}
		}
		
		return $failed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::delete()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $key
	 * @return boolean
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function delete($key) : bool
	{
		return $this->_psr16Cache->delete($this->buildKey($key));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::flush()
	 * @return boolean
	 */
	public function flush() : bool
	{
		return $this->_psr16Cache->clear();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::getOrSet()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $key
	 * @param (callable():(null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>))|(Closure():(null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>)) $callable
	 * @param ?int $duration
	 * @param ?Dependency $dependency
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function getOrSet($key, $callable, $duration = null, $dependency = null) : mixed
	{
		$key = $this->buildKey($key);
		if($this->exists($key))
		{
			return $this->get($key);
		}
		
		$value = $callable();
		if(false !== $value)
		{
			/** @psalm-suppress MixedArgument */
			$this->set($key, $value, $duration, $dependency);
		}
		
		return $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetExists()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $offset
	 * @return boolean
	 */
	public function offsetExists(mixed $offset) : bool
	{
		if(!\is_bool($offset) && !\is_int($offset) && !\is_float($offset) && !\is_string($offset) && !\is_object($offset))
		{
			return false;
		}
		
		try
		{
			return $this->exists($offset);
		}
		catch(InvalidArgumentException $exc)
		{
			return false;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetGet()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $offset
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function offsetGet(mixed $offset) : mixed
	{
		if(!\is_bool($offset) && !\is_int($offset) && !\is_float($offset) && !\is_string($offset) && !\is_object($offset))
		{
			return null;
		}
		
		try
		{
			return $this->get($offset);
		}
		catch(InvalidArgumentException $exc)
		{
			return null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetSet()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $offset
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 */
	public function offsetSet(mixed $offset, mixed $value) : void
	{
		if(!\is_bool($offset) && !\is_int($offset) && !\is_float($offset) && !\is_string($offset) && !\is_object($offset))
		{
			return;
		}
		
		try
		{
			$this->set($offset, $value);
		}
		catch(InvalidArgumentException $exc)
		{
			return;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetUnset()
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $offset
	 */
	public function offsetUnset(mixed $offset) : void
	{
		if(!\is_bool($offset) && !\is_int($offset) && !\is_float($offset) && !\is_string($offset) && !\is_object($offset))
		{
			return;
		}
		
		try
		{
			$this->delete($offset);
		}
		catch(InvalidArgumentException $exc)
		{
			return;
		}
	}
	
}
