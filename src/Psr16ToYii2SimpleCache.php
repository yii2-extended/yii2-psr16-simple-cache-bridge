<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-psr16-simple-cache-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Yii2SimpleCache;

use DateInterval;
use Stringable;
use yii\caching\CacheInterface;

/**
 * Psr16ToYii2SimpleCache class file.
 * 
 * This class transforms psr-16 compliant simple cache calls to yii2 compliant
 * cache calls.
 * 
 * @author Anastaszor
 */
class Psr16ToYii2SimpleCache implements \Psr\SimpleCache\CacheInterface, Stringable
{
	
	/**
	 * The yii2 compliant cache.
	 * 
	 * @var CacheInterface
	 */
	protected CacheInterface $_yii2Cache;
	
	/**
	 * Builds a new Psr16ToYii2SimpleCache object with the given inner psr-16
	 * simple cache.
	 * 
	 * @param CacheInterface $cache
	 */
	public function __construct(CacheInterface $cache)
	{
		$this->_yii2Cache = $cache;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::get()
	 * @param string $key
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $default
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @psalm-suppress MoreSpecificImplementedParamType,MixedInferredReturnType
	 */
	public function get($key, $default = null) : mixed
	{
		$res = $this->_yii2Cache->get($this->buildKey($key));
		if(false === $res)
		{
			return $default;
		}
		
		/** @psalm-suppress MixedReturnStatement */
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::set()
	 * @param string $key
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param null|int|DateInterval $ttl
	 * @return boolean
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function set($key, $value, $ttl = null) : bool
	{
		if($ttl instanceof DateInterval)
		{
			$secval = 0;
			$secval += 3600 * 24 * 365 * \abs($ttl->y);
			$secval += 3600 * 24 * 30 * \abs($ttl->m);
			$secval += 3600 * 24 * \abs($ttl->d);
			$secval += 3600 * \abs($ttl->h);
			$secval += 60 * \abs($ttl->i);
			$secval += \abs($ttl->s);
			$ttl = (int) $secval;
		}
		
		return $this->_yii2Cache->set($this->buildKey($key), $value, $ttl);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::delete()
	 * @param string $key
	 * @return boolean
	 */
	public function delete($key) : bool
	{
		return $this->_yii2Cache->delete($this->buildKey($key));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::clear()
	 * @return boolean
	 */
	public function clear() : bool
	{
		return $this->_yii2Cache->flush();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::getMultiple()
	 * @param iterable<null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $keys
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $default
	 * @return array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @psalm-suppress MoreSpecificImplementedParamType,InvalidReturnType,ImplementedReturnTypeMismatch
	 */
	public function getMultiple($keys, $default = null) : array
	{
		$nkeys = [];
		
		foreach($keys as $key)
		{
			if(!\is_array($key))
			{
				$key = [$key];
			}
			
			$keys = [];
			
			foreach($key as $nkey)
			{
				$keys[] = $this->buildKey($nkey);
			}
			
			$nkeys[] = \implode(',', $keys);
		}
		
		$res = $this->_yii2Cache->multiGet($nkeys);
		
		foreach($res as $k => $value)
		{
			if(false === $value)
			{
				$res[$k] = $default;
			}
		}
		
		/** @psalm-suppress InvalidReturnStatement */
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::setMultiple()
	 * @param iterable<null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $values
	 * @param null|int|DateInterval $ttl
	 * @return boolean
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function setMultiple($values, $ttl = null) : bool
	{
		if($ttl instanceof DateInterval)
		{
			$secval = 0;
			$secval += 3600 * 24 * 365 * \abs($ttl->y);
			$secval += 3600 * 24 * 30 * \abs($ttl->m);
			$secval += 3600 * 24 * \abs($ttl->d);
			$secval += 3600 * \abs($ttl->h);
			$secval += 60 * \abs($ttl->i);
			$secval += \abs($ttl->s);
			$ttl = (int) $secval;
		}
		
		$nvalues = [];
		
		foreach($values as $key => $value)
		{
			/** @psalm-suppress MixedArgument */
			$nvalues[$this->buildKey($key)] = $value;
		}
		
		return [] === $this->_yii2Cache->multiSet($nvalues, (int) $ttl);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::deleteMultiple()
	 * @param iterable<null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $keys
	 * @return boolean
	 */
	public function deleteMultiple($keys) : bool
	{
		$res = true;
		
		foreach($keys as $key)
		{
			$res = $res && $this->_yii2Cache->delete($this->buildKey($key));
		}
		
		return (bool) $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::has()
	 * @param string $key
	 * @return boolean
	 */
	public function has($key) : bool
	{
		return $this->_yii2Cache->exists($this->buildKey($key));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::buildKey()
	 * @param null|boolean|integer|float|string|object|resource|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $key
	 * @return string
	 */
	public function buildKey($key) : string
	{
		if(\is_array($key))
		{
			return \md5(\implode(':', \array_keys($key)));
		}
		
		if(\is_object($key))
		{
			return \md5(\spl_object_hash($key).\serialize($key));
		}
		
		if(\is_resource($key))
		{
			return \md5(\get_resource_type($key));
		}
		
		$strkey = (string) $key;
		if(\preg_match('#^[A-Za-z0-9_/+=-]+$#', $strkey))
		{
			return $strkey;
		}
		
		return \md5((string) $key);
	}
	
}
