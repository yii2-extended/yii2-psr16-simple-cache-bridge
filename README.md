# yii2-extended/yii2-psr16-simple-cache-bridge
A library to bridge psr-16 simple caches with the yii caching system.

![coverage](https://gitlab.com/yii2-extended/yii2-psr16-simple-cache-bridge/badges/master/pipeline.svg?style=flat-square) 
![build status](https://gitlab.com/yii2-extended/yii2-psr16-simple-cache-bridge/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar yii2-extended/yii2-psr16-simple-cache-bridge ^8`


## Basic Usage

This library offers one psr16 compliant simple cache to cache objects back to
yii2 caching system. As the psr16 cache system do not have any dependancy
mechanism, it will not use the one provided in yii's interfaces.

This library also offers one yii2 compliant cache to cache objects within any
psr-16 cache. Be warned that the current implementation does not implements the
dependancy mechanism yii's interfaces provides.

The `Psr16ToYii2SimpleCache` is straightforward to use, just look at the
[PSR-16 documentation](http://www.php-fig.org/psr/psr-16/).

```php

use Yii2Extended\Yii2SimpleCache\Psr16ToYii2SimpleCache;

$cache = new Psr16ToYii2SimpleCache(\Yii::$app->get('cache'));
$cache->get('key');
$cache->set('key', $value);

```

The `Yii2ToPsr16SimpleCache` is also simple to use but remember that 
the dependancy mechanism is not implemented.

Put in the `config/web.php` or `config/console.php`:

```

	'components' => [
		'cache_bridge' => [
			'class' => 'Yii2Extended\Yii2SimpleCache\Yii2ToPsr16SimpleCache',
			'cache' => function() { return \Yii::$app->get('cache'); },
		],
	],

```

And then in your code :

```

use Yii2Extended\Yii2SimpleCache\Yii2ToPsr16SimpleCache;

$cache = \Yii::$app->get('cache_bridge');
$cache->get('key');
$cache->set('key', $value);

```


## License

MIT (See [license file](LICENSE)).
