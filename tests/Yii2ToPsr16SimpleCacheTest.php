<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-psr16-simple-cache-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\CacheInterface;
use Yii2Extended\Yii2SimpleCache\Yii2ToPsr16SimpleCache;

/**
 * Yii2ToPsr16SimpleCacheTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Extended\Yii2SimpleCache\Yii2ToPsr16SimpleCache
 *
 * @internal
 *
 * @small
 */
class Yii2ToPsr16SimpleCacheTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Yii2ToPsr16SimpleCache
	 */
	protected Yii2ToPsr16SimpleCache $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Yii2ToPsr16SimpleCache($this->getMockForAbstractClass(CacheInterface::class));
	}
	
}
